import axios, {AxiosResponse} from "axios";

const getMovieById = (id=':id'): Promise<AxiosResponse> => {
    return axios.get(`https://api.tvmaze.com/shows/${id}`);
}

export default getMovieById;