import React from 'react';
import './main.css';

interface IInputParams {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    placeholder?: string;
    value?: string
}

const Input: React.FC<IInputParams> = ({onChange, placeholder, value}) => {
    return (
        <input type="text" className='search-input' value={value} onChange={onChange} placeholder={placeholder} />
    )
}

export default Input;