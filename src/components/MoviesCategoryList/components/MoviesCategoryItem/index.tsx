import React from 'react';
import { Link } from 'react-router-dom';
import { IMovies } from '../../../../types/IMovies';
import './main.css';

interface IMoviesCategoryItemParams {
    item: IMovies;
}

const MoviesCategoryItem: React.FC<IMoviesCategoryItemParams> = ({item}) => (
    <Link className='movies-category-item' to={`/MoviesDetailPage/${item.show.id}`}>
        <img src={item.show.image?.medium} alt={item.show.image?.medium}/>
        <div className='movies-category-item-info'>
            <p className='movies-category-item-title'>{item.show.name}</p>
            <p className='movies-category-item-genre'>{item.show.genres}</p>
        </div>
    </Link>
)

export default MoviesCategoryItem;