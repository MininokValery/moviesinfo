import React from 'react';
import MoviesCategoryItem from './components/MoviesCategoryItem';
import { IMovies } from '../../types/IMovies';
import './main.css';

interface IMoviesCategoryListParams {
    list: IMovies[];
}

const MoviesCategoryList: React.FC<IMoviesCategoryListParams> = ({list}) => (
    <div className='movies-category-list'>
        {list?.map((movies: IMovies) => (
            <MoviesCategoryItem key={movies.show.id} item={movies}/>
        ))}
    </div>
)

export default MoviesCategoryList;