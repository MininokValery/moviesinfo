import React from 'react';
import { IMoviesDetail } from '../../types/IMoviesDetail';
import './main.css';

interface IMoviesDetailParams {
    item: IMoviesDetail;
}

const MoviesDetail: React.FC<IMoviesDetailParams> = ({item}) => (
    <div className='movies-detail-wrapper'>
        <div className='movies-detail-image'>
            <img src={item.image?.medium ?? ''} alt={item.image?.medium ?? ''}/>
        </div>
        <div className='movies-detail-info'>
            <div className='movies-detail-info-header'>
                <p className='movies-detail-info-title'>{item.name}</p>
                <div className='movies-detail-info-rating'>
                    <p className='movies-detail-info-rating'>{item.rating.average} / 10</p>
                </div>
            </div>
            <div className='movies-detail-info-body'>
                <div className='movies-detail-info-text'>
                    <p>Год выхода:</p>
                    <span>{item.premiered}</span>
                </div>
                 <div className='movies-detail-info-text'>
                    <p>Страна:</p>
                     <span>{item.network?.country.name}</span>
                </div>
                <div className='movies-detail-info-text'>
                    <p>Жанр:</p>
                    <span>{item.genres[0]}</span>
                </div>
                <div className='movies-detail-info-text'>
                    <p>Язык:</p>
                    <span>{item.language}</span>
                </div>
                <div className='movies-detail-info-text'>
                    <p>Описание:</p>
                    <span>{item.summary}</span>
                </div>
            </div>
        </div>
    </div>
)

export default MoviesDetail;