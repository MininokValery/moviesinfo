import React from 'react';
import Vector from '../../assets/img/Vector.svg';
import './main.css';

interface IButtonParams {
    handleChange: () => void;
}

const Button: React.FC<IButtonParams> = ({handleChange}) => {
    return (
        <button
            type='button'
            className='search-button'
            onClick={handleChange}
        ><img src={Vector} alt={Vector} /></button>
    )
}

export default Button;