import React from 'react';
import MoviesItem from './components/MoviesItem';
import { IMovies } from '../../types/IMovies';
import './main.css';

interface IMoviesListParams {
    list: IMovies[];
}

const MoviesList: React.FC<IMoviesListParams> = ({list}) => (
    <div className='movies-list'>
        {list.map((movies: IMovies) => (
            <MoviesItem key={movies.show.id} item={movies}/>
        ))}
    </div>
)

export default MoviesList;