import React from 'react';
import { Link } from 'react-router-dom';
import { IMovies } from '../../../../types/IMovies';
import './main.css';

interface IMoviesItemParams {
    item: IMovies;
}

const MoviesItem: React.FC<IMoviesItemParams> = ({item}) => (
    <Link className='movies-item' to={`/MoviesDetailPage/${item.show.id}`}>
        <img src={item.show.image?.medium ?? ''} alt={item.show.image?.medium ?? ''}/>
        <p className='movies-title'>{item.show.name}</p>
        <p className='movies-genre'>{item.show.genres[0]}</p>
        <p className='movies-country'>{item.show.network?.country.name}</p>
    </Link>
)

export default MoviesItem;