import React from 'react';
import logo from '../../../assets/img/logo.png'

const Footer = () => {
    return (
        <footer className="footer">
        <div className="footer__container container">
            <p className="copy">
                <img src={logo} alt={'logo'}/> &copy;
            </p>
        </div>
    </footer>
    );
}

export default Footer;