import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../assets/img/logo.png';
import Navigation from './Navigation';
import MobileMenu from './MobileMenu';

const Header = () => {
    const navigation = [
        { id: '1', title: 'Главная', path: '/HomePage' },
    ];
    return (
        <header className="header">
            <div className="header__container container">
                <div className='header__logo'>
                    <Link to='/'>
                        <img src={logo} alt='logo'/>
                    </Link>
                </div>
                <Navigation navigation={navigation}/>
                <MobileMenu navigation={navigation}/>
            </div>
        </header>
    );
}

export default Header;