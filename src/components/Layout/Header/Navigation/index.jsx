import React from 'react';
import { NavLink } from "react-router-dom";

const Navigation = ({navigation}) => {
    return (
        <nav className='nav'>
            <ul className='nav__list'>
                {navigation.map(({id, title, path}) => (
                    <li key={id} className= 'nav__link'>
                        <NavLink to={path}>
                            {title}
                        </NavLink>
                    </li>
                ))}
            </ul>
        </nav>
    );
}

export default Navigation;