import React, { useState } from 'react';
import { NavLink } from "react-router-dom";

const MobileMenu = ({navigation}) => {
    const [isMenuClicked, setIsMenuClicked] = useState(false)
    const updateMenu = () => {
        if(!isMenuClicked) {
            document.body.classList.add("stop-scrolling");
        }
        else {
            document.body.classList.remove("stop-scrolling");
        }
        setIsMenuClicked(!isMenuClicked)
    }
    // const copyStyle = {
    //     display: 'block',
    //     fontSize: '1.3rem',
    //     textAlign: 'center',
    //     padding: '1rem 0'
    // }
    // const copyImageStyle = {
    //     width: '5rem',
    //     height: '2rem'
    // }
    return(
        <div className="mobile" onClick={updateMenu}>
            <div className="mobile-icon">
                <div className={`mobile-bar ${isMenuClicked ? 'clicked' : 'unclicked'}`} ></div>
                <div className={`mobile-bar ${isMenuClicked ? 'clicked' : 'unclicked'}`} ></div>
                <div className={`mobile-bar ${isMenuClicked ? 'clicked' : 'unclicked'}`} ></div>
            </div>
            <div className={`mobile-menu ${isMenuClicked ? 'visible' : 'hidden'}`}>
                <nav className="mobile-nav">
                    <ul className='mobile-nav__list'>
                        {navigation.map(({id, title, path}) => (
                            <li key={id} className= 'mobile-nav__link'>
                                <NavLink to={path}>
                                    {title}
                                </NavLink>
                            </li>
                        ))}
                    </ul>
                </nav>
                <a href='#' className="copy">
                    &copy; Project by Minin-Ok
                </a>
            </div>
        </div>
    )
}

export default MobileMenu;