import React from 'react';
import Footer from './Footer';
import Header from './Header';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import HomePage from '../../pages/HomePage';
import MoviesDetailPage from '../../pages/MoviesDetailPage';
import '../../styles/globals.css';

const Layout = () => {
    return (
        <div className="layout">
            <BrowserRouter>
                <Header/>
                <Routes>
                    <Route
                        path = '/'
                        element = {<HomePage/>}
                    />
                    <Route
                        path = '/HomePage'
                        element = {<HomePage/>}
                    />
                    {/* <Route
                        path = {routeMainMoviesDetailPage()}
                        element = {<MoviesDetailPage/>}
                    /> */}
                    <Route
                        path = '/MoviesDetailPage/:id'
                        element = {<MoviesDetailPage/>}
                    />
                </Routes>
                <Footer/>
            </BrowserRouter>
        </div>
    );
}

export default Layout;
