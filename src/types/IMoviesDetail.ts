import { INetwork } from "./INetwork"
import { IImage } from "./IImage"

export interface IMoviesDetail {
    id: string
    name: string
    language: string
    genres:[
        string
    ]
    premiered: string
    rating:{
        average: string
    }
    network: INetwork
    image: IImage
    summary: string
}