import { INetwork } from "./INetwork"
import { IImage } from "./IImage"

export interface IMovies {
    score: string
    show:{
        id: string
        name: string
        genres:[
            string
        ]
        network: INetwork
        image: IImage
    }
}