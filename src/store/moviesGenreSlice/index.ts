// import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { IMovies } from '../../types/IMovies';
import getMoviesByGenre from '../../services/getMoviesByGenre';

// const moviesGenreSlice = createSlice({
//     name: 'moviesGenre',
//     initialState:[] as IMovies[],
//     reducers: {
//         setMoviesGenre: (state, action:PayloadAction<IMovies>) => {
//             state.push(action.payload);
//         },
//     },
// });
interface MoviesState {
    movies: IMovies[];
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | null;
}
const initialState: MoviesState = {
    movies: [],
    status: 'idle',
    error: null,
};
const moviesGenreSlice = createSlice({
    name: 'moviesGenre',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
      builder
        .addCase(loadMoviesGenre.pending, (state) => {
          state.status = 'loading';
        })
        .addCase(loadMoviesGenre.fulfilled, (state, action) => {
          state.status = 'succeeded';
          state.movies = action.payload;
        })
        .addCase(loadMoviesGenre.rejected, (state, action) => {
          state.status = 'failed';
          state.error = action.error.message || 'Something went wrong';
        });
    }
});

export const loadMoviesGenre  = createAsyncThunk('moviesGenre/loadMoviesGenre ', async (genre: string) => {
    try {
        const response = await getMoviesByGenre(genre);
        return response.data as IMovies[];
    } catch (error) {
        throw new Error('Не удалось получить список фильмов');
    }
});

// export const { setMoviesGenre } = moviesGenreSlice.actions;
export default moviesGenreSlice.reducer;