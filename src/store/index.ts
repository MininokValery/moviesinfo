import { configureStore } from '@reduxjs/toolkit';
import moviesReducer from './moviesSlice';
import moviesDetailReducer from './moviesDetailSlice';
import moviesGenreReducer from './moviesGenreSlice';

const store = configureStore({
    reducer: {
        movies: moviesReducer,
        moviesDetail: moviesDetailReducer,
        moviesGenre: moviesGenreReducer,
    },
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store