import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { IMovies } from '../../types/IMovies';
import getMovieById from '../../services/getMovieById';

const moviesDetailSlice = createSlice({
    name: 'moviesDetail',
    initialState:[] as IMovies[],
    reducers: {
        setMoviesDetail: (state, action:PayloadAction<IMovies>) => {
            state.push(action.payload);
        },
    },
});

export const loadMoviesDetail   = createAsyncThunk('moviesDetail/loadMoviesDetail ', async (id: string) => {
    try {
        const response = await getMovieById(id=':id');
        moviesDetailSlice.actions.setMoviesDetail(response.data);
    } catch (error) {
        throw new Error('Не удалось получить список фильмов');
    }
});

// export const { setMoviesDetail } = moviesDetailSlice.actions;
export default moviesDetailSlice.reducer;