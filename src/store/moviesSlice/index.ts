// import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { IMovies } from '../../types/IMovies';
// import axios from 'axios';
import getMovies from '../../services/getMovies';

// const movieSlice = createSlice({
//   name: 'movies',
//   initialState: [] as IMovies[],
//   reducers: {
//     setMovies: (state, action:PayloadAction<IMovies>) => {
//       state.push(action.payload);
//     },
//   },
// });
interface MoviesState {
    movies: IMovies[];
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | null;
}

const initialState: MoviesState = {
    movies: [],
    status: 'idle',
    error: null,
};

const movieSlice = createSlice({
    name: 'movies',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
      builder
        .addCase(fetchMovies.pending, (state) => {
          state.status = 'loading';
        })
        .addCase(fetchMovies.fulfilled, (state, action) => {
          state.status = 'succeeded';
          state.movies = action.payload;
        })
        .addCase(fetchMovies.rejected, (state, action) => {
          state.status = 'failed';
          state.error = action.error.message || 'Something went wrong';
        });
    }
});

export const fetchMovies = createAsyncThunk('movies/fetchMovies', async () => {
    try {
        const response = await getMovies();
        return response.data as IMovies[];
    } catch (error) {
        throw new Error('Не удалось получить список фильмов');
    }
});

// export const fetchMovies = createAsyncThunk('movies/fetchMovies', async () => {
//   try {
//       const response = await axios.get("https://api.tvmaze.com/search/shows?q=girls");
//       movieSlice.actions.setMovies(response.data);
//   } catch (error) {
//       throw new Error('Не удалось получить список фильмов');
//   }
// });

// export const fetchMovies = createAsyncThunk('movies/fetchMovies', async (url: string) => {
//     const response = await fetch(url);
//     const data = await response.json();
//     return data as IStore[];
// });

// export const fetchMovies = createAsyncThunk('movies/fetchMovies', async () => {
//     const response = await axios.get('YOUR_API_URL_HERE');
//     return response.data;
// });
export default movieSlice.reducer;


// import { configureStore } from '@reduxjs/toolkit';
// import todoListReducer from './reducers/todoList';
// const store = configureStore({
//   reducer: {
//     todoList: todoListReducer,
//   },
// });
// export default store;


// import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// interface Todo {
//   id: number;
//   text: string;
//   completed: boolean;
// }
// interface TodoListState {
//   todos: Todo[];
// }
// const initialState: TodoListState = {
//   todos: [],
// };
// const todoListSlice = createSlice({
//   name: 'todoList',
//   initialState,
//   reducers: {
//     addTodo: (state, action: PayloadAction<Todo>) => {
//       state.todos.push(action.payload);
//     },
//     // Добавьте другие действия, связанные с вашим списком задач
//   },
// });
// export const { addTodo } = todoListSlice.actions;
// export default todoListSlice.reducer;


// import React from 'react';
// import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';
// import store from './store';
// import App from './App';

// ReactDOM.render(
//   <Provider store={store}>
//     <App />
//   </Provider>,
//   document.getElementById('root')
// );


// TypeScript: npx create-react-app my-app --template typescript
// Install Redux Toolkit and React-Redux: npm install @reduxjs/toolkit react-redux

// import { configureStore } from '@reduxjs/toolkit';
// import rootReducer from './reducers';

// const store = configureStore({
//   reducer: rootReducer,
// });

// export default store;


// import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// interface CounterState {
//   value: number;
// }
// const initialState: CounterState = {
//   value: 0,
// };
// const counterSlice = createSlice({
//   name: 'counter',
//   initialState,
//   reducers: {
//     increment: (state) => {
//       state.value += 1;
//     },
//     decrement: (state) => {
//       state.value -= 1;
//     },
//     incrementByAmount: (state, action: PayloadAction<number>) => {
//       state.value += action.payload;
//     },
//   },
// });
// export const { increment, decrement, incrementByAmount } = counterSlice.actions;
// export default counterSlice.reducer;


// import { combineReducers } from 'redux';
// import counterReducer from './reducers/counterSlice';

// const rootReducer = combineReducers({
//   counter: counterReducer,
//   // Add more reducers here if needed
// });

// export default rootReducer;


// import React from 'react';
// import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';
// import store from './store';
// import App from './App';

// ReactDOM.render(
//   <Provider store={store}>
//     <App />
//   </Provider>,
//   document.getElementById('root')
// );


// import React from 'react';
// import { useSelector, useDispatch } from 'react-redux';
// import { increment, decrement, incrementByAmount } from './reducers/counterSlice';

// const Counter = () => {
//   const counter = useSelector((state) => state.counter.value);
//   const dispatch = useDispatch();

//   const handleIncrement = () => {
//     dispatch(increment());
//   };

//   const handleDecrement = () => {
//     dispatch(decrement());
//   };

//   const handleIncrementByAmount = () => {
//     dispatch(incrementByAmount(5));
//   };

//   return (
//     <div>
//       <p>Counter: {counter}</p>
//       <button onClick={handleIncrement}>Increment</button>
//       <button onClick={handleDecrement}>Decrement</button>
//       <button onClick={handleIncrementByAmount}>Increment by 5</button>
//     </div>
//   );
// };

// export default Counter;
