import React, {useEffect, useState} from 'react';
import routeMain from './routes';
import { useAppSelector, useAppDispatch } from '../../hooks';
import { loadMoviesGenre } from '../../store/moviesGenreSlice';
import MoviesCategoryList from '../../components/MoviesCategoryList';
import Button from '../../components/Button';
import Input from '../../components/Input';
import './main.css';

const MoviesSearchPage = () => {
    const dispatch  = useAppDispatch();
    const moviesCategoryList = useAppSelector((state) => state.movies.movies);

    const [search, setSearch] = useState<string>('');
    const [searchResult, setSearchResult] = useState<string>('');

    useEffect(() => {
        dispatch(loadMoviesGenre(searchResult));
    }, [dispatch, searchResult])

    const handleChange = () => {
        setSearchResult(search);
        setSearch('')
    };

    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    }

    return (
        <section className='movies-search-page'>
            <div className='movies-search-page-container'>
                <h2 className='movies-search-page-title'>Поиск по категории</h2>
                <div className='movies-genre-search-wrapper'>
                    <Input
                        value={search}
                        onChange={onChange}
                        placeholder="example: animals"
                    />
                    <Button handleChange={handleChange}/>
                </div>
                <h3 className='movies-search-page-subtitle'>Результаты поиска: <span>{searchResult}</span></h3>
                {moviesCategoryList.length > 0 ? (
                    <MoviesCategoryList list={moviesCategoryList}/>
                    ) : (
                        <p className='movies-search-page-text'>
                            Введите поисковой запрос
                            <span>для отображения результатов</span>
                        </p>
                    )
                }
            </div>
        </section>
    )
}

export {routeMain};

export default MoviesSearchPage;