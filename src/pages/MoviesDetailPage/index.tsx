import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import routeMain from './routes';
// import { useSelector } from 'react-redux';
// import { selectMoviesDetail } from '../../store/moviesDetail/selectors';
// import { loadMoviesDetail } from '../../store/moviesDetail/actions';
// import { useTypedDispatch } from '../../store';
import MoviesDetail from '../../components/MoviesDetail';
import { ID } from '../../types/ID';
import { IMoviesDetail } from '../../types/IMoviesDetail';
import getMovieById from '../../services/getMovieById';
import './main.css';

const MoviesDetailPage = () => {
    const {id} = useParams<ID>();
    // const dispatch  = useTypedDispatch();
    const [movies, setMovies] = useState<IMoviesDetail | null>(null);
    // const [movies, setMovies] = useState<IMoviesDetail>({} as IMoviesDetail);!!!

    // const moviesDetail = useSelector(selectMoviesDetail);

    useEffect(() => {
        getMovieById(id).then(response => {
            setMovies(response.data);
        })
    }, [id])
    // console.log(moviesDetail)
    console.log(movies)

    // useEffect(() => {
    //     dispatch(loadMoviesDetail(id));
    // }, [dispatch, id])

    return (
        <section className='movies-detail-page'>
            <div className='movies-detail-container'>
                {movies ? (
                    <MoviesDetail item={movies}/>
                ): <h1>фильм не найден</h1>}
            </div>
        </section>
    )
}

export {routeMain};

export default MoviesDetailPage;