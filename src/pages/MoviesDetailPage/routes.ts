const routeMain = (id = ':id') => `/MoviesDetailPage/${id}`;

export default routeMain;