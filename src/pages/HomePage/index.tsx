import React, {useEffect} from 'react';
import { useAppSelector, useAppDispatch } from '../../hooks';
import { fetchMovies } from '../../store/moviesSlice';
import MoviesList from '../../components/MoviesList';
import './main.css';

const HomePage = () => {
    const dispatch = useAppDispatch();
    const moviesList = useAppSelector((state) => state.movies.movies);
    // const moviesList = useAppSelector((state) => state.movies);
    // const status = useAppSelector((state) => state.movies.status);
    // const error = useAppSelector((state) => state.movies.error);
    useEffect(() => {
        dispatch(fetchMovies());
    }, [ dispatch]);
    // useEffect(() => {
    //     if (status === 'idle') {
    //         dispatch(fetchMovies());
    //     }
    // }, [status, dispatch]);
    // if (status === 'loading') {
    //     return <div>Loading...</div>;
    // }
    // if (status === 'failed') {
    //     return <div>Error: {error}</div>;
    // }
    console.log(moviesList)
    return (
        <section className='home-page'>
            <h1 className='home-page-title'>MOVIESinfo</h1>
            <p className='home-page-text'>Самый популярный портал о фильмах</p>
            <div className='home-page-container'>
                {moviesList.length > 0 && <MoviesList list={moviesList.slice(0,8)}/>}
            </div>
        </section>
    )
}

export default HomePage;



// import React, { useEffect, useState } from 'react';
// import { useSelector, useDispatch } from 'react-redux';
// import { createSlice, configureStore } from '@reduxjs/toolkit';

// interface Movie {
//   id: number;
//   title: string;
//   genre: string;
// }

// const movieSlice = createSlice({
//   name: 'movies',
//   initialState: [] as Movie[],
//   initialState: Movie[] = [];
//   reducers: {
//     setMovies: (state, action) => {
//       return action.payload;
//     },
//   },
// });

// const store = configureStore({
//   reducer: movieSlice.reducer,
// });

// const MovieTheater: React.FC = () => {
//   const movies = useSelector((state: Movie[]) => state);
//   const dispatch = useDispatch();
//   const [showAllMovies, setShowAllMovies] = useState(false);

//   useEffect(() => {
//     fetchMovies();
//   }, []);

//   const fetchMovies = async () => {
//     try {
//       const response = await fetch('https://api.example.com/movies');
//       const data = await response.json();
//       dispatch(movieSlice.actions.setMovies(data));
//     } catch (error) {
//       console.log('Error fetching movies:', error);
//     }
//   };

//   const handleShowAllMovies = () => {
//     setShowAllMovies(true);
//   };

//   return (
//     <div>
//       <h1>Онлайн кинотеатр</h1>
//       <h2>Фильмы в прокате:</h2>
//       <ul>
//         {showAllMovies
//           ? movies.map((movie) => (
//               <li key={movie.id}>
//                 <strong>{movie.title}</strong> - {movie.genre}
//               </li>
//             ))
//           : movies.slice(0, 3).map((movie) => (
//               <li key={movie.id}>
//                 <strong>{movie.title}</strong> - {movie.genre}
//               </li>
//             ))}
//       </ul>
//       {!showAllMovies && (
//         <button onClick={handleShowAllMovies}>Показать все фильмы</button>
//       )}
//     </div>
//   );
// };

// const App: React.FC = () => {
//   return (
//     <Provider store={store}>
//       <MovieTheater />
//     </Provider>
//   );
// };

// export default App;




// import React, { useEffect } from 'react';
// import { useAppSelector, useAppDispatch } from '../../hooks';
// import { fetchMovies } from './moviesSlice';

// const MoviesList: React.FC = () => {
//     const dispatch = useAppDispatch();
//     const movies = useAppSelector((state) => state.movies);
//     const status = useAppSelector((state) => state.movies.status);
//     const error = useAppSelector((state) => state.movies.error);
//     useEffect(() => {
//         if (status === 'idle') {
//         dispatch(fetchMovies());
//         }
//     }, [status, dispatch]);
//     if (status === 'loading') {
//         return <div>Loading...</div>;
//     }
//     if (status === 'failed') {
//         return <div>Error: {error}</div>;
//     }
//     return (
//         <div>
//             <h2>Movies List</h2>
//             <ul>
//                 {movies.map((movie) => (
//                 <li key={movie.id}>{movie.title}</li>
//                 ))}
//             </ul>
//         </div>
//     );
// };
// export default MoviesList;
